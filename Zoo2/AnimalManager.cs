﻿using System;

namespace Zoo2
{
    public class AnimalManager
    {
        public AnimalRepository AnimalRepository { get; } = new AnimalRepository();

        public BoxRepository BoxRepository { get; }  = new BoxRepository();

        public void AssignBox(Guid animalId, Guid boxId)
        {
            var animal = AnimalRepository.GetAnimal(animalId);
            var box = BoxRepository.GetBox(boxId);

            animal.Box?.AnimalsInBox.Remove(animal);

            animal.Box = box;
            box.AnimalsInBox.Add(animal);
        }
    }
}