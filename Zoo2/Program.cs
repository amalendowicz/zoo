﻿using System;

namespace Zoo2
{
    class Program
    {
        private static AnimalManager _animalManager;

        static void Main(string[] args)
        {
            _animalManager = new AnimalManager();

            menu:
            WriteMenu();
            var decision = Console.ReadLine();
            DoOnDecision(decision);
            Console.ReadKey();
            goto menu;
        }

        private static void DoOnDecision(string decision)
        {
            if (decision == "1")
            {
                AddAnimal();
            }
            else if (decision == "2")
            {
                RemoveAnimal();
            }
            else if (decision == "3")
            {
                ListAnimals();
            }
            else if (decision == "4")
            {
                AddBox();
            }
            else if (decision == "5")
            {
                RemoveBox();
            }
            else if (decision == "6")
            {
                ListBoxes();
            }
            else if (decision == "7")
            {
                ListAnimalsInBox();
            }
            else if (decision == "8")
            {
                AssignBoxToAnimal();
            }
        }

        private static void AssignBoxToAnimal()
        {
            Console.WriteLine("Podaj id zwierzęcia");
            var animalId = Console.ReadLine();
            Console.WriteLine("Podaj id boksu");
            var boxId = Console.ReadLine();
            _animalManager.AssignBox(Guid.Parse(animalId), Guid.Parse(boxId));
        }

        private static void ListAnimalsInBox()
        {
            Console.WriteLine("Podaj ID boksu do wypisania zwierząt");
            var id = Console.ReadLine();
            _animalManager.BoxRepository.ListAnimalsInBox(Guid.Parse(id));
        }

        private static void ListBoxes()
        {
            _animalManager.BoxRepository.ListBoxes();
        }

        private static void RemoveBox()
        {
            Console.WriteLine("Podaj ID boksu do usunięcia");
            var id = Console.ReadLine();

            _animalManager.BoxRepository.RemoveBox(Guid.Parse(id));
        }

        private static void AddBox()
        {
            var id = _animalManager.BoxRepository.AddBox();
            Console.WriteLine($"Boks został utworzony z ID: {id}");
        }

        private static void ListAnimals()
        {
            _animalManager.AnimalRepository.ListAnimals();
        }

        private static void RemoveAnimal()
        {
            Console.WriteLine("Podaj ID zwierzęcia do usunięcia");
            var id = Console.ReadLine();

            _animalManager.AnimalRepository.RemoveAnimal(Guid.Parse(id));
        }

        private static void AddAnimal()
        {
            Console.WriteLine("Podaj nazwę zwierzęcia");
            var name = Console.ReadLine();

            Console.WriteLine("Podaj wiek zwierzęcia");
            var age = Console.ReadLine();

            var id = _animalManager.AnimalRepository.AddAnimal(name, Convert.ToUInt32(age));
            Console.WriteLine($"Zwierzę zostało utworzone z ID: {id}");
        }

        private static void WriteMenu()
        {
            Console.Clear();
            Console.WriteLine("Wybierz odpowiednią opcję:");
            Console.WriteLine("Zwierzęta:");
            Console.WriteLine("1. aby dodać zwierzę");
            Console.WriteLine("2. aby usunąć zwierzę");
            Console.WriteLine("3. aby wypisać wszystkie zwierzęta");

            Console.WriteLine("Klatki");
            Console.WriteLine("4. aby dodać klatkę");
            Console.WriteLine("5. aby usunąć klatkę");
            Console.WriteLine("6. aby wypisać wszystkie klatki");
            Console.WriteLine("7. aby wypisać wszystkie zwierzęta z danej klatki");

            Console.WriteLine("Zarządzanie");
            Console.WriteLine("8. przypisać zwierzę do klatki");
        }
    }
}