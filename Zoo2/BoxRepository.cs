﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Zoo2
{
    public class BoxRepository
    {
        private List<Box> _boxes = new List<Box>();

        public Guid AddBox()
        {
            var box = new Box();
            _boxes.Add(box);
            return box.Id;
        }

        public void RemoveBox(Guid id)
        {
            var box = GetBox(id);
            if (box == null)
            {
                Console.WriteLine("Boks nie zostało znaleziony w repozytorium");
                return;
            }

            foreach (var animal in box.AnimalsInBox)
            {
                animal.Box = null;
            }

            _boxes.Remove(box);
            Console.WriteLine($"Boks o id: {id} został usunięty.");
        }

        public void ListBoxes()
        {
            foreach (var box in _boxes)
            {
                Console.WriteLine($"Boks o id: {box.Id} ma {box.AnimalsInBox.Count} zwierząt");
            }
        }

        public void ListAnimalsInBox(Guid boxId)
        {
            var box = GetBox(boxId);
            Console.WriteLine($"Boks o id: {box.Id} ma {box.AnimalsInBox.Count} zwierząt takich jak:");

            foreach (var animal in box.AnimalsInBox)
            {
                Console.WriteLine(animal.ToString());
            }
        }

        public Box GetBox(Guid id)
        {
            var box = _boxes.FirstOrDefault(a => a.Id == id);
            return box;
        }
    }
}