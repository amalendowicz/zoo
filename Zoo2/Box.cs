﻿using System;
using System.Collections.Generic;

namespace Zoo2
{
    public class Box
    {
        public Box()
        {
            Id = Guid.NewGuid();
            AnimalsInBox = new List<Animal>();
        }

        public Guid Id { get; }

        public List<Animal> AnimalsInBox { get; }
    }
}