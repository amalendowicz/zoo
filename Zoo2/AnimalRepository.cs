﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Zoo2
{
    public class AnimalRepository
    {
        private List<Animal> _animals = new List<Animal>();

        public Guid AddAnimal(string name, uint age)
        {
            var animal = new Animal(name, age);
            _animals.Add(animal);
            return animal.Id;
        }

        public Animal GetAnimal(Guid id)
        {
            var animal = _animals.FirstOrDefault(a => a.Id == id);
            return animal;
        }

        public void RemoveAnimal(Guid id)
        {
            var animal = GetAnimal(id);
            if (animal == null)
            {
                Console.WriteLine("Zwierzę nie zostało znalezione w repozytorium");
                return;
            }

            if (animal.Box != null)
            {
                animal.Box.AnimalsInBox.Remove(animal);
            }

            _animals.Remove(animal);
            Console.WriteLine($"Zwierzę o id: {id} zostało usunięte.");
        }

        public void ListAnimals()
        {
            foreach (var animal in _animals)
            {
                Console.WriteLine(animal.ToString());
            }
        }
    }
}