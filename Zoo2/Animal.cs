﻿using System;

namespace Zoo2
{
    public class Animal
    {
        public Animal(string name, uint age)
        {
            Id = Guid.NewGuid();
            Name = name;
            Age = age;
        }

        public Guid Id { get; }

        public string Name { get; set; }

        public uint Age { get; set; }

        public Box Box { get; set; }

        public override string ToString()
        {
            var boxId = Box != null ? $"oraz ma id: {Box.Id.ToString()}" : null;
            return $"Zwierzę o id: {Id} nazywa się: {Name} i ma lat: {Age} {boxId}";
        }
    }
}